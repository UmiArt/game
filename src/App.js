import Board from "./Components/Board";
import React from "react";

const App = () => {

    return (
        <div className="wrapper">
            <Board />
        </div>
    )
}

export default App;
