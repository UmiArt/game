import React from 'react';
import './Search.css';
import Square from "./Square";
import {useState} from "react";
import './Search.css';

const Board = () => {
    const [board, setBoard] = useState(0);
    const [colors, setColors] = useState({mak: false});

    let array = [];
    for(let i = 0; i < 36; i++) {
        array.push(i)
    }

    let classNameDiv = ['square'];

    const  handleClick = () => {
        colors.mak = true
        setColors({colors})

        let currentBoard = board;
        currentBoard++;
        setBoard(currentBoard);

    }


    const randoms = () => {
        const random = Math.floor(Math.random() * array.length);
        const randomVal = array[random];

        for (let i = 0; i < array.length; i++) {
            if (array[i] === randomVal) {
                const yes = array[i] === randomVal

            }
        }
    }
    randoms()

    const newArray = array.map(newArr => (

        <Square key={newArr.toString()}
                squares={board}
                id={newArr}
                newClassNameDiv={classNameDiv}
                onClick={() => handleClick(newArr)}

        />
    ));


    return (
        <div className='box'>
            <div className="board">
                {newArray}
            </div>
            <p className='tries'>Tries : {board}</p>
            <button className="btn">Reset</button>
        </div>
    );
}

export default Board;