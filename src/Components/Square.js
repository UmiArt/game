import React from 'react';
import './Search.css';

const Square = (props)=> {

    return (

        <div className={props.newClassNameDiv.join(' ')}
             id={props.id} onClick={props.onClick}>
        </div>

    );
}

export default Square;